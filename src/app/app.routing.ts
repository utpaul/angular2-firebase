import {Routes, RouterModule} from "@angular/router";
import {DirectoryComponent} from "./directory/directory.component";
import {HomeComponent} from "./home/home.component";

const APP_ROUTES:Routes =[
  {path:'',redirectTo:'/home',pathMatch:'full'},
  {path:'home', component:HomeComponent},
  {path:'directory', component:DirectoryComponent}

];

export const routing= RouterModule.forRoot(APP_ROUTES);
