import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class DataserviceService {

  constructor(private http:Http) {}

  fatchData(){
      return this.http.get('https://angular2-b34e2.firebaseio.com/.json').map(
        (res) => res.json()
      );
  }

}
