import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DirectoryComponent } from './directory/directory.component';
import {routing} from "./app.routing";
import { FilterPipe } from './filter.pipe';
import { CapitalizePipe } from './capitalize.pipe';
import {DataserviceService} from "./dataservice.service";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DirectoryComponent,
    FilterPipe,
    CapitalizePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [DataserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
