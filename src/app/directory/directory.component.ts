import { Component, OnInit } from '@angular/core';
import {DataserviceService} from "../dataservice.service";
declare var firebase:any;

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.css']
})
export class DirectoryComponent implements OnInit {

  listing = [];

  constructor(private dataSvc:DataserviceService) {
    //console.log(this.listing);
  }

  ngOnInit() {

    /*this.dataSvc.fatchData().subscribe(
     (data)=> this.listing = data
   );*/

    this.fbGeting()
  }

  fbGeting(){
    firebase.database().ref('/').on('child_added',(snapshot) =>{
      this.listing.push(snapshot.val());
    })
  }

  fbPosting(name, belt){
    firebase.database().ref('/').push({name:name, belt:belt});
  }



}
