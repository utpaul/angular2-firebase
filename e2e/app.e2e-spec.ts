import { FirebaseAngularjsPage } from './app.po';

describe('firebase-angularjs App', () => {
  let page: FirebaseAngularjsPage;

  beforeEach(() => {
    page = new FirebaseAngularjsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
